from datetime import datetime
from abc import ABC, abstractmethod


def timer(method):
    """Decorator// measures the runtime of a sorting method """
    def inner(self):
        now = datetime.now()
        method(self)
        self.timer = datetime.now() - now
        return method(self)
    return inner


class AbstractSortAlgorithm(ABC):
    def __init__(self, unsorted, reverse=False):
        self.unsorted = unsorted
        self.reverse = reverse
        self.timer = None

    @abstractmethod
    def sort(self):
        pass

    def swap(self, i, j):
        self.unsorted[i], self.unsorted[j] = self.unsorted[j], self.unsorted[i]

    def compare(self, i, j, index=True):
        if index:
            result = self.unsorted[i] - self.unsorted[j]
        else:
            result = i - j
        if self.reverse:
            result *= -1
        return result

    @property
    def unsorted(self):
        return self._unsorted

    @unsorted.setter
    def unsorted(self, value):
        if len(value) <= 1:
            raise ValueError("List must contain more than 1 item")
        self._unsorted = value


class Bubble(AbstractSortAlgorithm):
    @timer
    def sort(self):
        changed = True
        while changed:
            changed = False
            for i in range(len(self.unsorted) - 1):
                if self.compare(i, i+1, index=True) > 0:
                    self.swap(i, i+1)
                    changed = True
        return self.unsorted


class Insertion(AbstractSortAlgorithm):
    @timer
    def sort(self):
        for i in range(1, len(self.unsorted)):
            cursor = self.unsorted[i]
            j = i - 1
            while j > -1 and self.compare(self.unsorted[j], cursor, index=False) > 0:
                self.unsorted[j + 1] = self.unsorted[j]
                j -= 1
            self.unsorted[j + 1] = cursor
        return self.unsorted


class Merge(AbstractSortAlgorithm):
    @timer
    def sort(self):
        def recursion(temp):
            if len(temp) <= 1:
                return temp
            mid = len(temp) // 2
            left, right = recursion(temp[:mid]), recursion(temp[mid:])
            return self.merge(left, right, temp.copy())
        return recursion(self.unsorted)

    def merge(self, left, right, merged):
        left_cursor, right_cursor = 0, 0
        while left_cursor < len(left) and right_cursor < len(right):
            if self.compare(right[right_cursor], left[left_cursor], index=False) > 0:
                merged[left_cursor + right_cursor] = left[left_cursor]
                left_cursor += 1
            else:
                merged[left_cursor + right_cursor] = right[right_cursor]
                right_cursor += 1

        for left_cursor in range(left_cursor, len(left)):
            merged[left_cursor + right_cursor] = left[left_cursor]

        for right_cursor in range(right_cursor, len(right)):
            merged[left_cursor + right_cursor] = right[right_cursor]
        return merged


class Selection(AbstractSortAlgorithm):
    @timer
    def sort(self):
        for i in range(len(self.unsorted)):
            minimum = i
            for j in range(i + 1, len(self.unsorted)):
                if self.compare(j, minimum) > 0:
                    minimum = j
            self.swap(minimum, i)
        return self.unsorted


class Quick(AbstractSortAlgorithm):
    @timer
    def sort(self):
        def recursion(left_cursor, right_cursor):
            if left_cursor < right_cursor:
                split_index = self.divider(left_cursor, right_cursor)
                recursion(left_cursor, split_index)
                recursion(split_index + 1, right_cursor)
            return self.unsorted
        return recursion(0, len(self.unsorted)-1)

    def divider(self, left_cursor, right_cursor):
        pivot = self.unsorted[(left_cursor + right_cursor) // 2]
        i = left_cursor - 1
        j = right_cursor + 1
        while True:
            i += 1
            while self.compare(self.unsorted[i], pivot, index=False) < 0:
                i += 1
            j -= 1
            while self.compare(self.unsorted[j], pivot, index=False) > 0:
                j -= 1
            if i >= j:
                return j
            self.swap(i, j)


