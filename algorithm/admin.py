from django.contrib import admin
from .models import Algorithm


class AlgorithmAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Algorithm._meta.fields]
    list_display_links = ['id', 'type']
    list_filter = ['type']
    list_per_page = 100
    search_fields = [field.name for field in Algorithm._meta.fields]


admin.site.register(Algorithm, AlgorithmAdmin)
