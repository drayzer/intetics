from django.db import models


class Algorithm(models.Model):
    TYPES = (('Bubble', 'Bubble'),
             ('Insertion', 'Insertion'),
             ('Merge', 'Merge'),
             ('Selection', 'Selection'),
             ('Quick', 'Quick'))
    type = models.CharField("Algorithm name", max_length=20, choices=TYPES, blank=False, null=False)
    unsorted = models.FileField()
    sorted = models.TextField("Sorted numbers", blank=False, null=False)
    reverse = models.BooleanField("Reverse sort", default=False)
    time = models.DurationField("Measured execution time")
    created_at = models.DateTimeField("Created at", auto_now=False, auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.type)

    class Meta:
        verbose_name = "Algorithm"
        verbose_name_plural = "Algorithms"
