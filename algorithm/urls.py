from django.urls import path
from .views import AlgorithmView

app_name = "home"

urlpatterns = [
    path('', AlgorithmView.as_view(), name="algorithm"),
]
