from django.shortcuts import render
from django.views.generic import View
from .forms import AlgorithmForm
from .utility import Bubble, Insertion, Merge, Selection, Quick


def handle_file(f):
    data = []
    with open(f.name, 'r+') as file:
        for line in file.read().splitlines():
            # Integers must be divided by comma or space
            if ',' in line:
                x = line.split(',')
            else:
                x = line.split()
            data.extend(x)
        try:
            data = [int(val) for val in data]
        except ValueError:
            data = None
        return data


class AlgorithmView(View):
    template_name = 'algorithm.html'
    form_class = AlgorithmForm

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            algorithm = form.save(commit=False)
            unsorted = handle_file(request.FILES['unsorted'])
            if unsorted:  # return False if invalid characters were found in the file
                # globals()["ClassName"] -> Bubble(unsorted=unsorted, reverse=algorithm.reverse)
                alg = globals()[algorithm.type](unsorted=unsorted, reverse=algorithm.reverse)
                result = alg.sort()
                algorithm.sorted = result
                algorithm.time = alg.timer
                algorithm.save()
            else:
                error = "File must contain only integers"
        return render(request, self.template_name, locals())
